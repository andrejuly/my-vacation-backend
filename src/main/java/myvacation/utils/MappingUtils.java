package myvacation.utils;

import lombok.Builder;
import myvacation.dto.*;
import myvacation.model.Note;
import myvacation.model.Trip;
import myvacation.model.User;
import myvacation.repository.TripRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;

@Service
@Builder
public class MappingUtils {

    @Autowired
    private TripRepository tripRepository;

    // From entity to dto
    public TripDTO mapToTripDTO(Trip trip) {
        if (trip == null)
            return null;
        return TripDTO.builder()
                .id(trip.getId())
                .destinations(trip.getDestinations())
                .endDate(trip.getEndDate())
                .startDate(trip.getStartDate())
                .name(trip.getName())
                .user(trip.getUser())
                .activeTrip(trip.isActiveTrip())
                .notes(trip.getNotes())
                .build();
    }

    // From entity to dto
    public TripInfoDTO mapToTripInfoDTO(TripDTO trip) {
        if (trip == null)
            return null;

        return TripInfoDTO.builder()
                .id(trip.getId())
                .destinations(trip.getDestinations())
                .endDate(trip.getEndDate())
                .startDate(trip.getStartDate())
                .name(trip.getName())
                .login(trip.getUser().getUserName())
                .activeTrip(trip.isActiveTrip())
                .build();
    }

    // From dto to entity
    public Trip mapToTrip(TripDTO dto) {

        return Trip.builder()
                .id(dto.getId())
                .destinations(dto.getDestinations())
                .endDate(dto.getEndDate())
                .startDate(dto.getStartDate())
                .notes(dto.getNotes())
                .name(dto.getName())
                .user(dto.getUser())
                .activeTrip(dto.isActiveTrip())
                .build();
    }

    public Note mapToNote(NoteDTO noteDTO) {
        return Note.builder()
                .content(noteDTO.getContent())
                .id(noteDTO.getId())
                .datetime(noteDTO.getDatetime())
                .trip(tripRepository.findTripById(noteDTO.getTripId()))
                .author(noteDTO.getAuthor())
                .build();
    }

    public NoteDTO mapToNoteDTO(Note note) {
        return NoteDTO.builder()
                .content(note.getContent())
                .id(note.getId())
                .datetime(note.getDatetime())
                .author(note.getAuthor())
                .build();
    }

    // From entity to dto
    public UserDTO mapToUsersDTO(User user) {
        if (user == null)
            return null;

        List<String> list = new ArrayList<>();
        user.getRoles().forEach(role ->
                list.add(role.getName()));

        return UserDTO
                .builder()
                .id(user.getId())
                .login(user.getUserName())
                .password(user.getPassword())
                .trips(user.getTrips())
                .sex(user.getSex())
                .fName(user.getFName())
                .birthDate(user.getBirthDate())
                .roles(list)
                .build();
    }

    // From dto to entity
    public User mapToUsers(UserDTO dto) {

        return User
                .builder()
                .id(dto.getId())
                .userName(dto.getLogin())
                .password(dto.getPassword())
                .trips(dto.getTrips())
                .fName(dto.getFName())
                .sex(dto.getSex())
                .birthDate(dto.getBirthDate())
                .build();
    }

    public UserInfoDTO mapToUsersInfoDTO(User user) {
        if (user == null)
            return null;

        return UserInfoDTO
                .builder()
                .id(user.getId())
                .login(user.getUserName())
                .sex(user.getSex())
                .fName(user.getFName())
                .birthDate(user.getBirthDate())
                .build();
    }
}
