package myvacation.service;

import myvacation.dto.UserDTO;
import myvacation.dto.UserInfoDTO;
import myvacation.model.Status;
import myvacation.model.User;
import myvacation.repository.UserRepository;
import myvacation.utils.MappingUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.stream.Collectors;

@Service
public class UserService {
    @Autowired
    UserRepository userRepository;

    @Autowired
    MappingUtils mappingUtils;

    private final BCryptPasswordEncoder encoder = new BCryptPasswordEncoder();

    public User findByLogin(String login) {
        return userRepository.findByUserName(login).orElse(null);
    }

    public User findByLoginAndPassword(UserDTO userDTO) {
        User user = userRepository.findByUserName(userDTO.getLogin()).orElse(null);
        if (user == null) {
            return null;
        }
        if (encoder.matches(userDTO.getPassword(), user.getPassword())) {
            return user;
        }
        return null;
    }

    public UserInfoDTO findByUserName(String login) {
        return mappingUtils.mapToUsersInfoDTO(userRepository.findByUserName(login).orElse(null));
    }



    public List<UserDTO> findAll() {
        return userRepository.findAll().stream()
                .map(mappingUtils::mapToUsersDTO)
                .collect(Collectors.toList());
    }

    public UserDTO findById(Long id) {
        return mappingUtils.mapToUsersDTO(
                userRepository.findById(id)
                        .orElse(new User())
        );
    }

    public String findNameById(Long id) {
        UserDTO userDTO = mappingUtils.mapToUsersDTO(
                userRepository.findById(id)
                        .orElse(new User()));
        return userDTO.getLogin();
    }

    public User patch(User user) {
        userRepository.save(user);
        return user;
    }

    public User save(User user) {
        user.setStatus(Status.ACTIVE);
        user.setPassword(encoder.encode(user.getPassword()));
        userRepository.save(user);
        user.setPassword(null);
        return user;

    }

}
