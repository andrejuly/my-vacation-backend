package myvacation.service;

import myvacation.model.Subscription;
import myvacation.repository.SubscriptionRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class SubscriptionService {

    @Autowired
    private SubscriptionRepository subscriptionRepository;

    public void addSubscription(Subscription subscription) {
        subscriptionRepository.save(subscription);
    }

    public void deleteSubscription(Subscription subscription) {
        Subscription tempSubscription = subscriptionRepository.findSubscriptionByUserIdAndSubscribedTo(
                subscription.getUserId(),
                subscription.getSubscribedTo());
        if(tempSubscription == null)
            return;

        subscriptionRepository.deleteById(tempSubscription.getId());
    }

    public Subscription findByUserIdAndSubscriberId(Long userId, Long subscriberId) {
        return subscriptionRepository.findSubscriptionByUserIdAndSubscribedTo(userId, subscriberId);
    }

}
