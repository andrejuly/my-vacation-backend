package myvacation.service;

import myvacation.dto.NewsDTO;
import myvacation.dto.TripDTO;
import myvacation.dto.TripInfoDTO;
import myvacation.dto.WeatherDTO;
import myvacation.model.Destination;
import myvacation.utils.MappingUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.web.client.RestTemplate;

import java.util.List;

@Service
public class ContentService {

    @Autowired
    private TripService tripService;

    @Autowired
    private MappingUtils mappingUtils;

    private final String apiKeyNews = "bdbced4a3b0f4339b6bc099c0eddc269";
    private final String apiKeyWeather = "d132f38327b7b7b2f9d2ead8e5c9fbbb";

    public TripInfoDTO findTrip(Long id) {
        TripDTO tripDTO = tripService.findTripByUserId(id);
        return mappingUtils.mapToTripInfoDTO(tripDTO);
    }

    public TripInfoDTO findTripById(Long id) {
        TripDTO tripDTO = tripService.findTripById(id);
        return mappingUtils.mapToTripInfoDTO(tripDTO);
    }

    public WeatherDTO getWeather(Long id, String city) {
        if (tripService.findTripById(id).getDestinations().get(0) == null) {
            return null;
        }
        RestTemplate restTemplate = new RestTemplate();
        String cityForWeather = tripService.findTripById(id).getDestinations().get(0).getCity();
        String url = city.equals("") ?
                "https://api.openweathermap.org/data/2.5/weather?units=metric&lang=ru&q=" +
                        cityForWeather
                        + "&appid=" + apiKeyWeather
                :
                "https://api.openweathermap.org/data/2.5/weather?units=metric&lang=ru&q=" +
                        city
                        + "&appid=" + apiKeyWeather;
        return restTemplate.getForEntity(url, WeatherDTO.class).getBody();
    }

    public NewsDTO getNews(Long id, String city) {
        if (tripService.findTripById(id).getDestinations().get(0) == null) {
            return null;
        }
        RestTemplate restTemplate = new RestTemplate();
        String url = city.equals("") ? "https://newsapi.org/v2/everything?language=ru&pageSize=40&q=" +
                tripService.findTripById(id).getDestinations().get(0).getCity() +
                "&apiKey=" + apiKeyNews
                :
                "https://newsapi.org/v2/everything?language=ru&pageSize=40&q=" +
                        city + "&apiKey=" + apiKeyNews;
        return restTemplate.getForEntity(url, NewsDTO.class).getBody();
    }

    public List<Destination> getDestinations(Long id) {
        TripDTO tripDTO = tripService.findTripByUserId(id);
        return tripDTO.getDestinations();
    }

}