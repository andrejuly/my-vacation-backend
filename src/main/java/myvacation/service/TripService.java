package myvacation.service;

import myvacation.dto.TripDTO;
import myvacation.dto.TripInfoDTO;
import myvacation.model.Trip;
import myvacation.repository.TripRepository;
import myvacation.utils.MappingUtils;
import net.minidev.json.JSONArray;
import net.minidev.json.parser.JSONParser;
import net.minidev.json.parser.ParseException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.io.ClassPathResource;
import org.springframework.core.io.Resource;
import org.springframework.data.domain.*;
import org.springframework.stereotype.Service;

import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.util.List;
import java.util.stream.Collectors;

@Service
public class TripService {

    @Autowired
    private TripRepository tripRepository;

    @Autowired
    private SubscriptionService subscriptionService;

    @Autowired
    private MappingUtils mappingUtils;

    public TripDTO addTrip(TripDTO dto) {
        if (dto.isActiveTrip()) {
            List<TripDTO> tripDTOList = findAllTripByUserId(dto.getUser().getId());
            for (TripDTO tripDTO : tripDTOList) {
                tripDTO.setActiveTrip(false);
                tripRepository.save(mappingUtils.mapToTrip(tripDTO));
            }
        }
        Trip trip = mappingUtils.mapToTrip(dto);
        return mappingUtils.mapToTripDTO(tripRepository.save(trip));
    }

    public TripDTO findTripByNameAndUserId(String name, Long id) {
        return mappingUtils.mapToTripDTO(tripRepository.findTripByNameAndUser_Id(name, id));
    }

    public TripDTO findTripById(Long id) {
        return mappingUtils.mapToTripDTO(tripRepository.findTripById(id));
    }

    public TripDTO patchTrip(TripDTO dto) {
        List<TripDTO> tripDTOList = findAllTripByUserId(dto.getUser().getId());
        for (TripDTO tripDTO : tripDTOList) {
            tripDTO.setActiveTrip(false);
            tripRepository.save(mappingUtils.mapToTrip(tripDTO));
        }
        dto.setActiveTrip(true);
        Trip trip = mappingUtils.mapToTrip(dto);
        return mappingUtils.mapToTripDTO(tripRepository.save(trip));
    }

    public Page<TripDTO> findAllTrip(Pageable pageable, String keyword, Long userId, String type) {
        Page<Trip> tripData = null;
        if (type.equals("all")) {
            if (keyword.equals("")) {
                tripData = tripRepository.findAll(pageable);
            } else {
                tripData = tripRepository.findTripsByNameContainsIgnoreCase(pageable, keyword);
            }
        }
        else if (type.equals("favorites")) {
            if (keyword.equals("")) {
                tripData = tripRepository.findSubscribtions(userId, pageable);
            } else {
                tripData = tripRepository.findSubscribtionsWithSearch(userId, pageable, keyword);
            }
        }
        List<TripDTO> tripDTOList = tripData.getContent().stream().map(mappingUtils::mapToTripDTO)
                .collect(Collectors.toList());
        tripDTOList.forEach(tripDTO -> tripDTO.setSubscribe(isSubscribe(tripDTO, userId)));
        return new PageImpl<TripDTO>(tripDTOList, pageable, tripData.getTotalElements());

    }

    public Page<TripDTO> findSubscribtions(Long userId, Pageable pageable, String keyword) {
        Page<Trip> pageSubscriptions;
        if (keyword.equals("")) {
            pageSubscriptions = tripRepository.findSubscribtions(userId, pageable);
        } else {
            pageSubscriptions = tripRepository.findSubscribtionsWithSearch(userId, pageable, keyword);
        }

        List<TripDTO> tripDTOList = pageSubscriptions
                .getContent()
                .stream()
                .map(mappingUtils::mapToTripDTO)
                .collect(Collectors.toList());
        return new PageImpl<TripDTO>(tripDTOList, pageable, pageSubscriptions.getTotalElements());
    }

    public TripDTO findTripByUserId(Long id) {
        return mappingUtils.mapToTripDTO(tripRepository.findByUser_IdAndActiveTripEquals(id, true));
    }

    public List<TripDTO> findAllTripByUserId(Long id) {
        return tripRepository.findAllByUser_IdOrderByActiveTripDesc(id).stream()
                .map(mappingUtils::mapToTripDTO)
                .collect(Collectors.toList());
    }

    public List<TripInfoDTO> findTripsByUserId(Long id, String keyword) {
        List<TripDTO> list = keyword.equals("") ?
                tripRepository.findAllByUser_IdOrderByActiveTripDesc(id)
                        .stream()
                        .map(mappingUtils::mapToTripDTO)
                        .collect(Collectors.toList())
                :
                tripRepository.findAllByUser_IdAndNameContainsIgnoreCaseOrderByActiveTripDesc(id, keyword)
                        .stream()
                        .map(mappingUtils::mapToTripDTO)
                        .collect(Collectors.toList());
        return list.stream().map(mappingUtils::mapToTripInfoDTO).collect(Collectors.toList());
    }

    public JSONArray getCities() {
        JSONArray jsonArray = new JSONArray();
        try {
            Resource resource = new ClassPathResource("Cities1.json");
            InputStream resourceInputStream = resource.getInputStream();
            JSONParser jsonParser = new JSONParser();
            jsonArray = (JSONArray) jsonParser.parse(
                    new InputStreamReader(resourceInputStream, "UTF-8"));
        } catch (IOException | ParseException e) {
            e.printStackTrace();
        }

        return jsonArray;
    }

    private boolean isSubscribe(TripDTO tripDTO, Long userId) {
        return subscriptionService.findByUserIdAndSubscriberId(userId, tripDTO.getId()) != null;
    }

}

