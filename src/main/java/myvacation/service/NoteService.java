package myvacation.service;

import myvacation.dto.NoteDTO;
import myvacation.dto.TripDTO;
import myvacation.model.Note;
import myvacation.model.Subscription;
import myvacation.model.Trip;
import myvacation.repository.NoteRepository;
import myvacation.utils.MappingUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.stream.Collectors;

@Service
public class NoteService {
    @Autowired
    private NoteRepository noteRepository;

    @Autowired
    private MappingUtils mappingUtils;

    public Page<NoteDTO> getNotesByTripId(Long tripId, Pageable pageable) {
        Page<Note> page = noteRepository
                .findAllByTripIdOrderByDatetimeDesc(tripId,pageable);
        List<NoteDTO> notesDTOList = page
                .getContent()
                .stream()
                .map(mappingUtils::mapToNoteDTO)
                .collect(Collectors.toList());
        return new PageImpl<NoteDTO>(notesDTOList, pageable, page.getTotalElements());
    }

    public Note addNote(NoteDTO noteDTO) {
        System.out.println("noteDTO" + noteDTO);
        Note note = mappingUtils.mapToNote(noteDTO);
        System.out.println(note);
        return noteRepository.save(note);
    }

}
