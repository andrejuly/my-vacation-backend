package myvacation.security;

import lombok.extern.slf4j.Slf4j;
import myvacation.model.User;
import myvacation.security.jwt.JwtUser;
import myvacation.security.jwt.JwtUserFactory;
import myvacation.service.UserService;
import myvacation.utils.MappingUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.stereotype.Service;


@Service
@Slf4j
public class JwtUserDetailsService implements UserDetailsService {

    @Autowired
    private final UserService userService;

    @Autowired
    public JwtUserDetailsService(UserService userService, MappingUtils mappingUtils) {
        this.userService = userService;
    }

    @Override
    public UserDetails loadUserByUsername(String login) throws UsernameNotFoundException {

        User user = userService.findByLogin(login);

        if (user == null) {
            log.info("In loadUserByUsername - user with login {} not found", login);
            throw new UsernameNotFoundException("User with login: " + login + " not found");
        }

        JwtUser jwtUser = JwtUserFactory.create(user);
        log.info("In loadUserByUsername - user with login {} successfully loaded", login);
        return jwtUser;
    }
}

