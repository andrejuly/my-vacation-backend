package myvacation.controller;

import myvacation.dto.NoteDTO;
import myvacation.model.Note;
import myvacation.service.NoteService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Pageable;
import org.springframework.data.web.PageableDefault;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

@RestController
@RequestMapping("/notes")
public class NoteController {
    @Autowired
    private NoteService noteService;

    @PostMapping("/add")
    public ResponseEntity<Note> addNotes(@RequestBody NoteDTO noteDTO) {
        return ResponseEntity.ok(noteService.addNote(noteDTO));
    }

    @GetMapping("/getbytrip")
    public ResponseEntity<?> getAllNotesByTrip(@PageableDefault(page=0, size=3) Pageable pageable,
                                               @RequestParam(value = "tripid") Long id) {
        return ResponseEntity.ok(noteService.getNotesByTripId(id, pageable));
    }
}
