package myvacation.controller;

import myvacation.dto.ChangeTripDTO;
import myvacation.dto.TripDTO;
import myvacation.service.TripService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.web.PageableDefault;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

@RestController
public class TripController {

    @Autowired
    private TripService tripService;

    @PostMapping(value = "/addtrip")
    public ResponseEntity<TripDTO> addTrip(@RequestBody TripDTO tripDTO) {
        if (tripService.findTripByNameAndUserId(tripDTO.getName(), tripDTO.getUser().getId()) == null) {
            return ResponseEntity.ok(tripService.addTrip(tripDTO));
        }
        else {
            return new ResponseEntity<>(HttpStatus.CONFLICT);
        }
    }

    @GetMapping(value = "/trips")
    public Page<TripDTO> findAllTrip(@PageableDefault(page=0, size=6, sort = {"name"}) Pageable pageable,
                                                @RequestParam("filter") String filter,
                                                @RequestParam("userid") Long userId,
                                                @RequestParam(value = "type",defaultValue = "all") String type
                                                ) {

        return tripService.findAllTrip(pageable, filter, userId, type);

    }

    @GetMapping(value = "/cities")
    public ResponseEntity<?> getCities() {
        return ResponseEntity.ok(tripService.getCities());
    }

    @GetMapping(value = "/usertrips")
    public ResponseEntity<?> getUserTrips(@RequestParam Long userid, @RequestParam String keyword) {
        return ResponseEntity.ok(tripService.findTripsByUserId(userid, keyword));
    }

    @PostMapping("/statustrip")
    public ResponseEntity<TripDTO> changeStatusTrip(@RequestBody ChangeTripDTO changeTripDTO) {
        TripDTO tripDTO = tripService.findTripByNameAndUserId(changeTripDTO.getTripname(), changeTripDTO.getUserid());
        tripService.patchTrip(tripDTO);
        return ResponseEntity.ok(tripDTO);
    }
}
