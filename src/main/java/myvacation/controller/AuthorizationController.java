package myvacation.controller;

import myvacation.dto.UserDTO;
import myvacation.model.User;
import myvacation.security.jwt.JwtTokenProvider;
import myvacation.service.UserService;
import myvacation.utils.MappingUtils;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.authentication.BadCredentialsException;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.AuthenticationException;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import javax.validation.Valid;

@RestController
@RequestMapping("/authorization")
public class AuthorizationController {

    private final AuthenticationManager authenticationManager;

    private final JwtTokenProvider jwtTokenProvider;

    private final UserService userService;

    private final MappingUtils mappingUtils;

    public AuthorizationController(AuthenticationManager authenticationManager, JwtTokenProvider jwtTokenProvider,
                                   UserService userService, MappingUtils mappingUtils) {
        this.authenticationManager = authenticationManager;
        this.jwtTokenProvider = jwtTokenProvider;
        this.userService = userService;
        this.mappingUtils = mappingUtils;
    }

    @PostMapping("/register")
    public ResponseEntity<User> registerUser(@Valid @RequestBody UserDTO registerUser) {
        if (userService.findByLogin(registerUser.getLogin()) == null){
            User user = mappingUtils.mapToUsers(registerUser);
            userService.save(user);
            return ResponseEntity.ok(user);
        } else {
            return new ResponseEntity<>(HttpStatus.CONFLICT);
        }
    }


    @PostMapping(value = "/login")
    public ResponseEntity<UserDTO> loginUser(@Valid @RequestBody UserDTO userDTO) {
        try {
            String login = userDTO.getLogin();
            authenticationManager.authenticate(new UsernamePasswordAuthenticationToken(login, userDTO.getPassword()));
            User user = userService.findByLoginAndPassword(userDTO);
            if (user == null) {
                throw new UsernameNotFoundException("User with username: " + login + " not found");
            }

            userService.patch(user);
            String token = jwtTokenProvider.createToken(login, user.getRoles());
            UserDTO userDTO1 = mappingUtils.mapToUsersDTO(user);
            userDTO1.setToken(token);
            return ResponseEntity.ok(userDTO1);
        } catch (AuthenticationException e) {
            throw new BadCredentialsException("Invalid username or password");
        }
    }

}



