package myvacation.controller;

import myvacation.dto.UserDTO;
import myvacation.dto.UserInfoDTO;
import myvacation.service.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@ResponseBody
public class UserController {
        @Autowired
        private UserService userService;

        @GetMapping(value = "/users")
        public List<UserDTO> getUsers() {
            return userService.findAll();
        }

        @GetMapping(value = "/username")
        public ResponseEntity<String> getUsername(@RequestParam Long userid) {
                return ResponseEntity.ok(userService.findNameById(userid));
        }

        @GetMapping(value = "/user")
        public ResponseEntity<UserInfoDTO> getUsername(@RequestParam(value = "username") String username) {
                return ResponseEntity.ok(userService.findByUserName(username));
        }

}
