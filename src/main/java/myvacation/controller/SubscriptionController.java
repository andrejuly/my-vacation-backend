package myvacation.controller;

import myvacation.dto.TripDTO;
import myvacation.model.Subscription;
import myvacation.service.SubscriptionService;
import myvacation.service.TripService;
import myvacation.service.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.web.PageableDefault;
import org.springframework.messaging.handler.annotation.MessageMapping;
import org.springframework.messaging.handler.annotation.Payload;
import org.springframework.messaging.simp.SimpMessagingTemplate;
import org.springframework.web.bind.annotation.*;

@RestController
@RequestMapping(value = "/subscriptions")
public class SubscriptionController {

    @Autowired
    private SubscriptionService subscriptionService;

    @Autowired
    private UserService userService;

    @Autowired
    private TripService tripService;

    @Autowired
    private SimpMessagingTemplate simpMessagingTemplate;

    @MessageMapping("/websocket")
    @PostMapping("/add")
    public void setSubscription(@RequestBody Subscription subscription) {
        subscriptionService.addSubscription(subscription);
        String username = userService.findById(subscription.getUserId()).getLogin();
        simpMessagingTemplate.convertAndSendToUser(
                tripService.findTripById(
                        subscription.getSubscribedTo())
                        .getUser()
                        .getId().toString(),
                "/queue/messages","К вашей поездке присоединился " + username);
    }

    @PostMapping("/delete")
    public void deleteSubscription(@RequestBody Subscription subscription) {
        subscriptionService.deleteSubscription(subscription);
    }

    @GetMapping(value = "/allsubscriptions")
    public Page<TripDTO> getSubscriptions(@PageableDefault(page=0, size=6, sort = {"name"}) Pageable pageable,
                                          @RequestParam(value = "userid") Long userid,
                                          @RequestParam(value = "keyword") String keyword) {
        return tripService.findSubscribtions(userid, pageable, keyword);
    }

}
