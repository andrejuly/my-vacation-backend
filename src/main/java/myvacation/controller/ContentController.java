package myvacation.controller;

import myvacation.service.ContentService;
import myvacation.service.TripService;
import net.aksingh.owmjapis.api.APIException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

@RestController
@RequestMapping("/trip")
@ResponseBody
public class ContentController {

    @Autowired
    private ContentService contentService;

    @GetMapping(value = "weather")
    public ResponseEntity<?> getWeather(@RequestParam Long tripid,
                                        @RequestParam(defaultValue = "") String city) throws APIException {
        return ResponseEntity.ok(contentService.getWeather(tripid, city));
    }

    @GetMapping(value = "news")
    public ResponseEntity<?> getNews(@RequestParam Long tripid,
                                     @RequestParam(defaultValue = "") String city) throws APIException {
        return ResponseEntity.ok(contentService.getNews(tripid, city));
    }

    @GetMapping(value = "userid")
    public ResponseEntity<?> getTrip(@RequestParam Long userid) {
        return ResponseEntity.ok(contentService.findTrip(userid));
    }

    @GetMapping(value = "/{id}")
    public ResponseEntity<?> getTripById(@PathVariable("id") Long id) {
        return ResponseEntity.ok(contentService.findTripById(id));
    }

    @GetMapping(value = "destinations")
    public ResponseEntity<?> getDestinations(@RequestParam Long userid) {
        return ResponseEntity.ok(contentService.getDestinations(userid));
    }

}
