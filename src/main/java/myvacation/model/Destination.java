package myvacation.model;

import com.fasterxml.jackson.annotation.JsonIgnore;
import lombok.*;

import javax.persistence.*;
import java.util.List;

@Entity
@Table (name = "Destinations")
@Builder
@NoArgsConstructor
@AllArgsConstructor
@Setter
@Getter
public class Destination {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    private String city;

    private Float lat;

    private Float lon;

    @JsonIgnore
    @ManyToMany(cascade=CascadeType.ALL)
    @JoinTable(name="TripToUser", joinColumns = @JoinColumn(name = "destination_id"),
            inverseJoinColumns=@JoinColumn(name="trip_id"))
    private List<Trip> trips;

}
