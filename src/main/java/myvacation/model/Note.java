package myvacation.model;

import com.fasterxml.jackson.annotation.JsonIgnore;
import lombok.*;

import javax.persistence.*;
import java.util.Date;

@Entity
@Table(name = "Notes")
@Builder
@NoArgsConstructor
@AllArgsConstructor
@Setter
@Getter
public class Note {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    private String content;

    private String author;

    private Date datetime;

    @JsonIgnore
    @ManyToOne
    @JoinColumn (name = "trip_id")
    private Trip trip;

}
