package myvacation.model;

import lombok.*;

import javax.persistence.*;
import java.util.Date;
import java.util.List;

@Builder
@NoArgsConstructor
@AllArgsConstructor
@Setter
@Getter
@Entity
@Table(name = "Trips")
public class Trip {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @Column(name = "start_date", nullable = false)
    private Date startDate;
    @Column(name = "end_date", nullable = false)
    private Date endDate;

    private String name;

    private int likes;

    @OneToMany(mappedBy = "trip", cascade=CascadeType.ALL)
    private List<Note> notes;

    @ManyToOne
    @JoinColumn (name = "user_id")
    private User user;

    @ManyToMany(cascade=CascadeType.ALL)
    @JoinTable(name="TripToUser", joinColumns = @JoinColumn(name = "trip_id"),
            inverseJoinColumns=@JoinColumn(name="destination_id"))
    private List<Destination> destinations;

    private boolean activeTrip;

}
