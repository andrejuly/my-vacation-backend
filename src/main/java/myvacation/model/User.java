package myvacation.model;

import com.fasterxml.jackson.annotation.JsonIgnore;
import lombok.*;

import javax.persistence.*;
import java.util.Date;
import java.util.List;

@Entity
@Table(name = "Users", uniqueConstraints = @UniqueConstraint(columnNames = {"login"}))
@Builder
@Getter
@NoArgsConstructor
@AllArgsConstructor
public class User {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Setter
    private Long id;

    @JsonIgnore
    @Setter
    @OneToMany(mappedBy = "user", cascade=CascadeType.ALL)
    private List<Trip> trips;

    @Column(name = "login")
    @Setter
    private String userName;

    @Column(name = "password")
    @Setter
    private String password;

    @Column(name="fname")
    private String fName;

    private String sex;

    private Date birthDate;

    @Setter
    @Enumerated(EnumType.STRING)
    private Status status;

    @ManyToMany(fetch = FetchType.EAGER)
    @JoinTable(name = "user_roles",
            joinColumns = {@JoinColumn(name = "user_id", referencedColumnName = "id")},
            inverseJoinColumns = {@JoinColumn(name = "role_id", referencedColumnName = "id")})
    private List<Role> roles;

}