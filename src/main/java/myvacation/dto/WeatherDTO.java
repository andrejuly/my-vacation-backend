package myvacation.dto;

import lombok.Data;

import java.util.List;

@Data
public class WeatherDTO {

    private String name;

    private List<Weather> weather;

    private Main main;

    private Wind wind;


    @Data
    static class Weather {
        private String main;
        private String description;
        private String icon;
    }

    @Data
    static class Main {
        private Integer temp;
        private Integer humidity;
    }

    @Data
    static class Wind {
        private Integer speed;
    }
}

