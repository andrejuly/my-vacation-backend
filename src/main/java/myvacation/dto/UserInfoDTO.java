package myvacation.dto;

import lombok.*;

import java.util.Date;

@Builder
@Data
@NoArgsConstructor
@AllArgsConstructor
@Setter
@Getter
public class UserInfoDTO {
    private Long id;
    private String login;
    private String sex;
    private String fName;
    private Date birthDate;
}
