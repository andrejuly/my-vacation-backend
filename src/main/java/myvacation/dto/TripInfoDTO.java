package myvacation.dto;

import lombok.*;
import myvacation.model.Destination;

import java.util.Date;
import java.util.List;

@Builder
@Data
@NoArgsConstructor
@AllArgsConstructor
@Setter
@Getter
public class TripInfoDTO {

    private Long id;

    private Date startDate;
    private Date endDate;

    private boolean activeTrip;

    private String name;

    private String login;

    private List<Destination> destinations;

}