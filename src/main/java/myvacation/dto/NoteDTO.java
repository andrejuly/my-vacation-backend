package myvacation.dto;

import lombok.*;
import myvacation.model.Trip;

import java.util.Date;

@Data
@Builder
@AllArgsConstructor
@NoArgsConstructor
public class NoteDTO {
    @Getter
    @Setter
    private Long id;

    @Getter
    @Setter
    private String content;

    @Getter
    @Setter
    private String author;

    @Getter
    @Setter
    private Date datetime;

    private Long tripId;
}
