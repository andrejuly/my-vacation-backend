package myvacation.dto;

import lombok.*;
import myvacation.model.Role;
import myvacation.model.Trip;

import javax.validation.constraints.Email;
import javax.validation.constraints.NotBlank;
import javax.validation.constraints.Pattern;
import java.util.Date;
import java.util.List;

@Builder
@Data
@NoArgsConstructor
@AllArgsConstructor
@Setter
@Getter
public class UserDTO {
    private Long id;

    @Pattern(regexp = "^[a-zA-Z]{4,15}$")
    @NotBlank
    private String login;

    @Pattern(regexp = "^(?=.*\\d)(?=.*[a-z])(?=.*[A-Z]).{6,}")
    @NotBlank
    private String password;

    private String sex;

    private String fName;

    private Date birthDate;

    private List<Trip> trips;

    private String token;

    private Date update;

    private List<String> roles;

}
