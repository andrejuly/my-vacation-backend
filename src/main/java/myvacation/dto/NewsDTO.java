package myvacation.dto;

import lombok.Data;

import java.util.List;

@Data
public class NewsDTO {

    private String status;
    private Integer totalResults;
    private List<Articles> articles;

    @Data
    public static class Articles {
        private String title;
        private String description;
        private String url;
    }
}
