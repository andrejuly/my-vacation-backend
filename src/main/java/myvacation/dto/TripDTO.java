package myvacation.dto;

import lombok.*;
import myvacation.model.Destination;
import myvacation.model.Note;
import myvacation.model.User;

import java.util.Date;
import java.util.List;

@Builder
@Data
@NoArgsConstructor
@AllArgsConstructor
@Setter
@Getter
public class TripDTO {

    private Long id;

    private Date startDate;
    private Date endDate;

    private boolean activeTrip;

    private String name;

    private User user;

    private List<Destination> destinations;

    private List<Note> notes;

    private boolean subscribe;

}
