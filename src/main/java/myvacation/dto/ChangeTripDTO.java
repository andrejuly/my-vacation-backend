package myvacation.dto;

import lombok.Data;

@Data
public class ChangeTripDTO {
    private String tripname;
    private Long userid;
}
