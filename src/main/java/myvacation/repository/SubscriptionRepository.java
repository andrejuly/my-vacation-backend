package myvacation.repository;

import myvacation.model.Subscription;
import org.springframework.data.jpa.repository.JpaRepository;

public interface SubscriptionRepository extends JpaRepository<Subscription, Long> {
    Subscription findSubscriptionByUserIdAndSubscribedTo(Long userId, Long subscriberId);
}
