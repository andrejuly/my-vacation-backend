package myvacation.repository;

import myvacation.model.Trip;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

import java.util.List;

public interface TripRepository extends JpaRepository<Trip,Long> {
    Trip findByUser_IdAndActiveTripEquals(Long id, boolean flag);
    Trip findByUser_Id(Long id);
    List<Trip> findAllByUser_IdOrderByActiveTripDesc(Long id);
    List<Trip> findAllByUser_IdAndNameContainsIgnoreCaseOrderByActiveTripDesc(Long id, String name);
    Trip findTripByNameAndUser_Id(String name, Long id);
    Trip findTripById(Long id);

    @Query(value = "select * from trips where " +
            "id in (select subscribed_to from " +
            "subscriptions where user_id = :id)" ,nativeQuery = true)
    Page<Trip> findSubscribtions(@Param(value = "id")Long id, Pageable pageable);

    @Query(value = "select * from trips where " +
            "id in (select subscribed_to from " +
            "subscriptions where user_id = :id " +
            "and " +
            "trips.name ilike '%' || :keyword || '%')" ,nativeQuery = true)
    Page<Trip> findSubscribtionsWithSearch(@Param(value = "id")Long id,
                                           Pageable pageable,
                                           @Param(value = "keyword") String keyword);

    Page<Trip> findTripsByNameContainsIgnoreCase(Pageable pageable, String name);

}
