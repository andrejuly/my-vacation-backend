package myvacation.repository;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import myvacation.model.Note;

public interface NoteRepository extends JpaRepository<Note, Long> {
    Page<Note> findAllByTripIdOrderByDatetimeDesc(Long tripId, Pageable pageable);
}
